<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class AdGetMessage extends BaseCreateMessage
{
    private string $login;
    private int $adId;

    public function __construct(string $login, int $adId, string $guid)
    {
        parent::__construct($guid);
        $this->login = $login;
        $this->adId = $adId;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getAdId(): int
    {
        return $this->adId;
    }
}
