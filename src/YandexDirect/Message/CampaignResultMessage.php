<?php

declare(strict_types=1);

namespace YandexDirect\Message;

use YandexDirect\ResultFields\ActionResult;

final class CampaignResultMessage extends BaseResultMessage
{
    private array $actionResults;

    public function __construct(array $actionResults, string $guid)
    {
        parent::__construct($guid);
        $this->actionResults = $actionResults;
    }

    /**
     * @return array|ActionResult[]
     */
    public function getActionResults(): array
    {
        return $this->actionResults;
    }
}
