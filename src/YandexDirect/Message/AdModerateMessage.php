<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class AdModerateMessage extends BaseCreateMessage
{
    /** @var array|int[] */
    private array $adIds;
    private string $login;

    public function __construct(string $login, array $adIds, string $guid)
    {
        parent::__construct($guid);
        $this->adIds = $adIds;
        $this->login = $login;
    }

    /**
     * @return array|int[]
     */
    public function getAdIds(): array
    {
        return $this->adIds;
    }

    public function getLogin(): string
    {
        return $this->login;
    }
}
