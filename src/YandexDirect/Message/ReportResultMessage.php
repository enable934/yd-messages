<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class ReportResultMessage extends BaseResultMessage
{
    private string $data;

    public function __construct(string $data, string $guid)
    {
        parent::__construct($guid);
        $this->data = $data;
    }

    public function getData(): string
    {
        return $this->data;
    }
}
