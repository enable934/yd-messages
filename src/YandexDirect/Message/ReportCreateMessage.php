<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class ReportCreateMessage extends BaseCreateMessage
{
    private string $login;
    private int $adId;
    private \DateTimeImmutable $dateFrom;
    private \DateTimeImmutable $dateTo;

    public function __construct(\DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo, int $adId, string $login, string $guid)
    {
        parent::__construct($guid);
        $this->login = $login;
        $this->adId = $adId;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getAdId(): int
    {
        return $this->adId;
    }

    public function getDateFrom(): \DateTimeImmutable
    {
        return $this->dateFrom;
    }

    public function getDateTo(): \DateTimeImmutable
    {
        return $this->dateTo;
    }
}
