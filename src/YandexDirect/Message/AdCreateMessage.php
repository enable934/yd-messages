<?php

declare(strict_types=1);

namespace YandexDirect\Message;

use YandexDirect\Fields\Ad\AdAddItem;

final class AdCreateMessage extends BaseCreateMessage
{
    private array $adAddItems;
    private string $login;

    public function __construct(array $adAddItems, string $login, string $guid)
    {
        parent::__construct($guid);
        $this->adAddItems = $adAddItems;
        $this->login = $login;
    }

    /**
     * @return array|AdAddItem[]
     */
    public function getAdAddItems(): array
    {
        return $this->adAddItems;
    }

    public function getLogin(): string
    {
        return $this->login;
    }
}
