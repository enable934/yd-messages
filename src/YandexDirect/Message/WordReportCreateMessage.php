<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class WordReportCreateMessage extends BaseCreateMessage
{
    private array $phrases;
    private array $geoId;

    public function __construct(array $phrases, array $geoId, string $guid)
    {
        $this->phrases = $phrases;
        parent::__construct($guid);
        $this->geoId = $geoId;
    }

    public function getPhrases(): array
    {
        return $this->phrases;
    }

    /**
     * @return array|int[]
     */
    public function getGeoId(): array
    {
        return $this->geoId;
    }
}
