<?php

declare(strict_types=1);

namespace YandexDirect\Message;

use YandexDirect\ResultFields\Region\RegionInfo;

final class RegionResultMessage
{
    private array $data;
    public const REGION_TYPES = [
        'Continent' => 'Continent',
        'Region' => 'Region',
        'Country' => 'Country',
        'Administrative area' => 'Administrative area',
        'City' => 'City',
        'Village' => 'Village',
    ];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array|RegionInfo[]
     */
    public function getData(): array
    {
        return $this->data;
    }
}
