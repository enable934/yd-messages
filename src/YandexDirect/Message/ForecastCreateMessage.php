<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class ForecastCreateMessage extends BaseCreateMessage
{
    public const CURRENCIES = [
        'RUB' => 'RUB', 'CHF' => 'CHF', 'EUR' => 'EUR', 'KZT' => 'KZT', 'TRY' => 'TRY', 'UAH' => 'UAH', 'USD' => 'USD', 'BYN' => 'BYN',
    ];
    private array $phrases;
    private string $currency;
    private array $geoId;

    public function __construct(array $phrases, string $currency, array $geoId, string $guid)
    {
        $this->phrases = $phrases;
        $this->currency = $currency;
        parent::__construct($guid);
        $this->geoId = $geoId;
    }

    public function getPhrases(): array
    {
        return $this->phrases;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return array|int[]
     */
    public function getGeoId(): array
    {
        return $this->geoId;
    }
}
