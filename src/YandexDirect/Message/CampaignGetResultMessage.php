<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class CampaignGetResultMessage extends BaseResultMessage
{
    public const STATES = [
        'CONVERTED' => 'Кампания велась в у. е. до перехода рекламодателя на работу в реальной валюте, в настоящее время перемещена в специальный архив и доступна только для чтения.',
        'ARCHIVED' => 'Кампания помещена в архив с помощью метода archive, пользователем в веб-интерфейсе или автоматически (если на счете нет средств и показов не было более 30 дней).',
        'SUSPENDED' => 'Кампания остановлена владельцем с помощью метода suspend или в веб-интерфейсе.',
        'ENDED' => 'Кампания закончилась (прошла дата окончания).',
        'ON' => 'Кампания активна, объявления могут быть показаны.',
        'OFF' => 'Кампания неактивна (черновик, ожидает модерации, отклонена, отсутствуют средства на кампании или на общем счете, нет активных объявлений).',
        'UNKNOWN' => 'Используется для обеспечения обратной совместимости и отображения состояний, не поддерживаемых в данной версии API.',
    ];
    public const STATUSES = [
      'DRAFT' => 'Кампания создана и еще не отправлена на модерацию.',
      'MODERATION' => 'Кампания находится на модерации.',
      'UNKNOWN' => 'Используется для обеспечения обратной совместимости и отображения статусов, не поддерживаемых в данной версии API.',
      'ACCEPTED' => 'Хотя бы одно объявление в кампании принято модерацией.',
      'REJECTED' => 'Все объявления в кампании отклонены модерацией.',
    ];
    private string $state;
    private string $status;
    private int $id;

    public function __construct(string $state, string $status, int $id, string $guid)
    {
        parent::__construct($guid);
        $this->state = $state;
        $this->status = $status;
        $this->id = $id;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
