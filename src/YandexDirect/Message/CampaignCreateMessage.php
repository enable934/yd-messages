<?php

declare(strict_types=1);

namespace YandexDirect\Message;

use YandexDirect\Fields\Campaign\CampaignAddItem;

final class CampaignCreateMessage extends BaseCreateMessage
{
    private array $campaignAddItems;
    private string $login;

    public function __construct(array $campaignAddItems, string $login, string $guid)
    {
        parent::__construct($guid);
        $this->campaignAddItems = $campaignAddItems;
        $this->login = $login;
    }

    /**
     * @return array|CampaignAddItem[]
     */
    public function getCampaignAddItems(): array
    {
        return $this->campaignAddItems;
    }

    public function getLogin(): string
    {
        return $this->login;
    }
}
