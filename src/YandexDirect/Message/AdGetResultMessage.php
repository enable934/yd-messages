<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class AdGetResultMessage extends BaseResultMessage
{
    public const STATES = [
        'SUSPENDED' => 'Показы объявления остановлены владельцем с помощью метода suspend или в веб-интерфейсе.',
        'OFF_BY_MONITORING' => 'Показы объявления автоматически остановлены мониторингом доступности сайта.',
        'ON' => 'Объявление активно, принадлежит к активной кампании и может быть показано (при наличии средств на кампании, в соответствии с настройками временного таргетинга и т. п.).',
        'OFF' => 'Объявление неактивно (черновик, ожидает модерации, отклонено) или принадлежит к неактивной либо остановленной кампании.',
        'ARCHIVED' => 'Объявление помещено в архив (с помощью метода archive или пользователем в веб-интерфейсе) или принадлежит к архивной кампании.',
    ];
    public const STATUSES = [
      'DRAFT' => 'Объявление создано и еще не отправлено на модерацию.',
      'MODERATION' => 'Объявление находится на модерации.',
      'PREACCEPTED' => 'Объявление допущено к показам автоматически, но будет дополнительно проверено модератором.',
      'ACCEPTED' => 'Объявление принято модерацией.',
      'REJECTED' => 'Объявление отклонено модерацией.',
    ];
    private string $state;
    private string $status;
    private int $id;

    public function __construct(string $state, string $status, int $id, string $guid)
    {
        parent::__construct($guid);
        $this->state = $state;
        $this->status = $status;
        $this->id = $id;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
