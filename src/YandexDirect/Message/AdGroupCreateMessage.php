<?php

declare(strict_types=1);

namespace YandexDirect\Message;

use YandexDirect\Fields\AdGroup\AdGroupAddItem;

final class AdGroupCreateMessage extends BaseCreateMessage
{
    private array $adGroupAddItems;
    private string $login;

    public function __construct(array $adGroupAddItems, string $login, string $guid)
    {
        parent::__construct($guid);
        $this->adGroupAddItems = $adGroupAddItems;
        $this->login = $login;
    }

    /**
     * @return array|AdGroupAddItem[]
     */
    public function getAdGroupAddItems(): array
    {
        return $this->adGroupAddItems;
    }

    public function getLogin(): string
    {
        return $this->login;
    }
}
