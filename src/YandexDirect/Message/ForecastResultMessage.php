<?php

declare(strict_types=1);

namespace YandexDirect\Message;

use YandexDirect\ResultFields\Forecast\BannerPhraseInfo;
use YandexDirect\ResultFields\Forecast\ForecastCommonInfo;

final class ForecastResultMessage extends BaseResultMessage
{
    private array $data;
    private ForecastCommonInfo $common;

    public function __construct(array $data, ForecastCommonInfo $common, string $guid)
    {
        $this->data = $data;
        $this->common = $common;
        parent::__construct($guid);
    }

    /**
     * @return array|BannerPhraseInfo[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function getCommon(): ForecastCommonInfo
    {
        return $this->common;
    }
}
