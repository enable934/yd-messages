<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class CampaignGetMessage extends BaseCreateMessage
{
    private string $login;
    private int $campaignId;

    public function __construct(string $login, int $campaignId, string $guid)
    {
        parent::__construct($guid);
        $this->login = $login;
        $this->campaignId = $campaignId;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getCampaignId(): int
    {
        return $this->campaignId;
    }
}
