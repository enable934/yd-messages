<?php

declare(strict_types=1);

namespace YandexDirect\Message;

use YandexDirect\ResultFields\WordReport\WordReportInfo;

final class WordReportResultMessage extends BaseResultMessage
{
    private array $data;

    public function __construct(array $data, string $guid)
    {
        $this->data = $data;
        parent::__construct($guid);
    }

    /**
     * @return array|WordReportInfo[]
     */
    public function getData(): array
    {
        return $this->data;
    }
}
