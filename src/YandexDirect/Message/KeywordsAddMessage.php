<?php

declare(strict_types=1);

namespace YandexDirect\Message;

final class KeywordsAddMessage extends BaseCreateMessage
{
    private array $phrases;
    private int $adGroupId;
    private string $login;

    public function __construct(string $login, array $phrases, int $adGroupId, string $guid)
    {
        parent::__construct($guid);
        $this->phrases = $phrases;
        $this->adGroupId = $adGroupId;
        $this->login = $login;
    }

    public function getAdGroupId(): int
    {
        return $this->adGroupId;
    }

    /**
     * @return array|string[]
     */
    public function getPhrases(): array
    {
        return $this->phrases;
    }

    public function getLogin(): string
    {
        return $this->login;
    }
}
