<?php

declare(strict_types=1);

namespace YandexDirect\Fields\AdGroup;

final class AdGroupAddItem
{
    private string $name;
    private int $campaignId;
    private array $regionIds;

    public function __construct(string $name, int $campaignId, array $regionIds)
    {
        $this->name = $name;
        $this->campaignId = $campaignId;
        $this->regionIds = $regionIds;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCampaignId(): int
    {
        return $this->campaignId;
    }

    /**
     * @return array|int[]
     */
    public function getRegionIds(): array
    {
        return $this->regionIds;
    }
}
