<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign;

final class TextCampaignStrategyAdd
{
    private TextCampaignSearchStrategyAdd $textCampaignSearchStrategyAdd;
    private TextCampaignNetworkStrategyAdd $textCampaignNetworkStrategyAdd;

    public function __construct(TextCampaignSearchStrategyAdd $textCampaignSearchStrategyAdd, TextCampaignNetworkStrategyAdd $textCampaignNetworkStrategyAdd)
    {
        $this->textCampaignSearchStrategyAdd = $textCampaignSearchStrategyAdd;
        $this->textCampaignNetworkStrategyAdd = $textCampaignNetworkStrategyAdd;
    }

    public function getTextCampaignSearchStrategyAdd(): TextCampaignSearchStrategyAdd
    {
        return $this->textCampaignSearchStrategyAdd;
    }

    public function getTextCampaignNetworkStrategyAdd(): TextCampaignNetworkStrategyAdd
    {
        return $this->textCampaignNetworkStrategyAdd;
    }
}
