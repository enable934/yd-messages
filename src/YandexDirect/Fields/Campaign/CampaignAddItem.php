<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign;

use DateTimeInterface;

final class CampaignAddItem
{
    private TextCampaignAddItem $textCampaignAddItem;
    private string $name;
    private DateTimeInterface $startDate;

    public function __construct(TextCampaignAddItem $textCampaignAddItem, string $name, DateTimeInterface $startDate)
    {
        $this->textCampaignAddItem = $textCampaignAddItem;
        $this->name = $name;
        $this->startDate = $startDate;
    }

    public function getTextCampaignAddItem(): TextCampaignAddItem
    {
        return $this->textCampaignAddItem;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStartDate(): string
    {
        return $this->startDate->format('Y-m-d');
    }
}
