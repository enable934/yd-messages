<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign;

use YandexDirect\Fields\Campaign\TextCampaignStrategy\BaseTextCampaignStrategy;
use YandexDirect\Fields\Campaign\TextCampaignStrategy\TextCampaignStrategyAverageCpa;
use YandexDirect\Fields\Campaign\TextCampaignStrategy\TextCampaignStrategyAverageCpc;
use YandexDirect\Fields\Campaign\TextCampaignStrategy\TextCampaignStrategyAverageRoi;
use YandexDirect\Fields\Campaign\TextCampaignStrategy\TextCampaignStrategyMaximumClicks;
use YandexDirect\Fields\Campaign\TextCampaignStrategy\TextCampaignStrategyMaximumConversionRate;
use YandexDirect\Fields\Campaign\TextCampaignStrategy\TextCampaignStrategyWeeklyClickPackage;

final class TextCampaignSearchStrategyAdd
{
    public const BIDDING_STRATEGY_TYPES = [
        'AVERAGE_CPA' => 'AVERAGE_CPA',
        'AVERAGE_ROI' => 'AVERAGE_ROI',
        'AVERAGE_CPC' => 'AVERAGE_CPC',
        'HIGHEST_POSITION' => 'HIGHEST_POSITION',
        'SERVING_OFF' => 'SERVING_OFF',
        'WB_MAXIMUM_CLICKS' => 'WB_MAXIMUM_CLICKS',
        'WB_MAXIMUM_CONVERSION_RATE' => 'WB_MAXIMUM_CONVERSION_RATE',
        'WEEKLY_CLICK_PACKAGE' => 'WEEKLY_CLICK_PACKAGE',
    ];
    private string $biddingStrategyType;
    private BaseTextCampaignStrategy $textCampaignStrategy;

    public function __construct(string $biddingStrategyType, BaseTextCampaignStrategy $textCampaignStrategy)
    {
        $this->biddingStrategyType = $biddingStrategyType;
        $this->textCampaignStrategy = $textCampaignStrategy;
        $this->checkCompatibility($biddingStrategyType, $textCampaignStrategy);
    }

    public function getBiddingStrategyType(): string
    {
        return $this->biddingStrategyType;
    }

    public function getTextCampaignStrategy(): BaseTextCampaignStrategy
    {
        return $this->textCampaignStrategy;
    }

    private function checkCompatibility(string $biddingStrategyType, BaseTextCampaignStrategy $textCampaignStrategy): void
    {
        if (($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['WB_MAXIMUM_CLICKS'] && !$textCampaignStrategy instanceof TextCampaignStrategyMaximumClicks)
            or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['WB_MAXIMUM_CONVERSION_RATE'] && !$textCampaignStrategy instanceof TextCampaignStrategyMaximumConversionRate)
            or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['AVERAGE_CPC'] && !$textCampaignStrategy instanceof TextCampaignStrategyAverageCpc)
            or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['AVERAGE_CPA'] && !$textCampaignStrategy instanceof TextCampaignStrategyAverageCpa)
            or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['WEEKLY_CLICK_PACKAGE'] && !$textCampaignStrategy instanceof TextCampaignStrategyWeeklyClickPackage)
            or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['AVERAGE_ROI'] && !$textCampaignStrategy instanceof TextCampaignStrategyAverageRoi)) {
            throw new \InvalidArgumentException(sprintf('Not valid text campaign strategy for bidding type %s', $biddingStrategyType));
        }
    }
}
