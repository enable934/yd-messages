<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignStrategy;

final class TextCampaignStrategyAverageCpc extends BaseTextCampaignStrategy
{
    private int $averageCpc;
    private ?int $weeklySpendLimit;

    public function __construct(int $averageCpc, int $weeklySpendLimit = null)
    {
        $this->averageCpc = $averageCpc;
        $this->weeklySpendLimit = $weeklySpendLimit;
    }

    public function getData(): array
    {
        $data = ['AverageCpc' => $this->averageCpc];

        if (null !== $this->weeklySpendLimit) {
            $data = $data + ['WeeklySpendLimit' => $this->weeklySpendLimit];
        }

        return $data;
    }

    public function getName(): string
    {
        return 'AverageCpc';
    }
}
