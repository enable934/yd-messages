<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignStrategy;

final class TextCampaignStrategyMaximumConversionRate extends BaseTextCampaignStrategy
{
    private int $weeklySpendLimit;
    private int $goalId;
    private ?int $bidCeiling;

    public function __construct(int $weeklySpendLimit, int $goalId, int $bidCeiling = null)
    {
        $this->weeklySpendLimit = $weeklySpendLimit;
        $this->goalId = $goalId;
        $this->bidCeiling = $bidCeiling;
    }

    public function getData(): array
    {
        $data = [
            'WeeklySpendLimit' => $this->weeklySpendLimit,
            'GoalId' => $this->goalId,
        ];

        if (null !== $this->bidCeiling) {
            $data = $data + ['BidCeiling' => $this->bidCeiling];
        }

        return $data;
    }

    public function getName(): string
    {
        return 'WbMaximumConversionRate';
    }
}
