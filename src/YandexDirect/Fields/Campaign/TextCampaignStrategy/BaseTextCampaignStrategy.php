<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignStrategy;

abstract class BaseTextCampaignStrategy
{
    abstract public function getData(): array;

    abstract public function getName(): string;
}
