<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy;

final class TextCampaignNetworkStrategyAverageRoi extends BaseTextCampaignNetworkStrategy
{
    private int $reserveReturn;
    private int $roiCoef;
    private int $goalId;
    private ?int $weeklySpendLimit;
    private ?int $bidCeiling;
    private ?int $profitability;

    public function __construct(int $reserveReturn, int $roiCoef, int $goalId, int $weeklySpendLimit = null, int $bidCeiling = null, int $profitability = null)
    {
        $this->reserveReturn = $reserveReturn;
        $this->roiCoef = $roiCoef;
        $this->goalId = $goalId;
        $this->weeklySpendLimit = $weeklySpendLimit;
        $this->bidCeiling = $bidCeiling;
        $this->profitability = $profitability;
    }

    public function getData(): array
    {
        $data = [
            'ReserveReturn' => $this->reserveReturn,
            'RoiCoef' => $this->roiCoef,
            'GoalId' => $this->goalId,
        ];

        if (null !== $this->weeklySpendLimit) {
            $data = $data + ['WeeklySpendLimit' => $this->weeklySpendLimit];
        }

        if (null !== $this->bidCeiling) {
            $data = $data + ['BidCeiling' => $this->bidCeiling];
        }

        if (null !== $this->profitability) {
            $data = $data + ['Profitability' => $this->profitability];
        }

        return $data;
    }

    public function getName(): string
    {
        return 'AverageRoi';
    }
}
