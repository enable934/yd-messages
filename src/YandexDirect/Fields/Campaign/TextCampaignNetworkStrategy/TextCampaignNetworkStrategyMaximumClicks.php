<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy;

final class TextCampaignNetworkStrategyMaximumClicks extends BaseTextCampaignNetworkStrategy
{
    private int $weeklySpendLimit;
    private ?int $bidCeiling;

    public function __construct(int $weeklySpendLimit, int $bidCeiling = null)
    {
        $this->weeklySpendLimit = $weeklySpendLimit;
        $this->bidCeiling = $bidCeiling;
    }

    public function getData(): array
    {
        $data = [
            'WeeklySpendLimit' => $this->weeklySpendLimit,
        ];

        if (null !== $this->bidCeiling) {
            $data = $data + ['BidCeiling' => $this->bidCeiling];
        }

        return $data;
    }

    public function getName(): string
    {
        return 'WbMaximumClicks';
    }
}
