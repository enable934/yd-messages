<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy;

final class TextCampaignNetworkStrategyAverageCpa extends BaseTextCampaignNetworkStrategy
{
    private int $averageCpa;
    private int $goalId;
    private ?int $weeklySpendLimit;
    private ?int $bidCeiling;

    public function __construct(int $averageCpa, int $goalId, int $weeklySpendLimit = null, int $bidCeiling = null)
    {
        $this->averageCpa = $averageCpa;
        $this->goalId = $goalId;
        $this->weeklySpendLimit = $weeklySpendLimit;
        $this->bidCeiling = $bidCeiling;
    }

    public function getData(): array
    {
        $data = [
            'AverageCpa' => $this->weeklySpendLimit,
            'GoalId' => $this->goalId,
        ];

        if (null !== $this->bidCeiling) {
            $data = $data + ['BidCeiling' => $this->bidCeiling];
        }

        if (null !== $this->weeklySpendLimit) {
            $data = $data + ['WeeklySpendLimit' => $this->weeklySpendLimit];
        }

        return $data;
    }

    public function getName(): string
    {
        return 'AverageCpa';
    }
}
