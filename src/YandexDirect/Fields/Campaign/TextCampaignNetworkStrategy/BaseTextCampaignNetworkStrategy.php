<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy;

abstract class BaseTextCampaignNetworkStrategy
{
    abstract public function getData(): array;

    abstract public function getName(): string;
}
