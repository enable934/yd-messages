<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy;

final class TextCampaignNetworkStrategyDefault extends BaseTextCampaignNetworkStrategy
{
    private int $limitPercent;

    public function __construct(int $limitPercent)
    {
        $this->limitPercent = $limitPercent;
    }

    public function getData(): array
    {
        return [
            'LimitPercent' => $this->limitPercent,
        ];
    }

    public function getName(): string
    {
        return 'NetworkDefault';
    }
}
