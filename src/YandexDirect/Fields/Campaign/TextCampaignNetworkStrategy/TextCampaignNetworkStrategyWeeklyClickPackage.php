<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy;

final class TextCampaignNetworkStrategyWeeklyClickPackage extends BaseTextCampaignNetworkStrategy
{
    private int $clicksPerWeek;
    private ?int $averageCpc;
    private ?int $bidCeiling;

    public function __construct(int $clicksPerWeek, int $averageCpc = null, int $bidCeiling = null)
    {
        $this->clicksPerWeek = $clicksPerWeek;
        $this->averageCpc = $averageCpc;
        $this->bidCeiling = $bidCeiling;
    }

    public function getData(): array
    {
        $data = [
            'ClicksPerWeek' => $this->clicksPerWeek,
        ];

        if (null !== $this->averageCpc) {
            $data = $data + ['AverageCpc' => $this->averageCpc];
        }

        if (null !== $this->bidCeiling) {
            $data = $data + ['BidCeiling' => $this->bidCeiling];
        }

        return $data;
    }

    public function getName(): string
    {
        return 'WeeklyClickPackage';
    }
}
