<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign;

use YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy\BaseTextCampaignNetworkStrategy;
use YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy\TextCampaignNetworkStrategyAverageCpa;
use YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy\TextCampaignNetworkStrategyAverageCpc;
use YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy\TextCampaignNetworkStrategyAverageRoi;
use YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy\TextCampaignNetworkStrategyDefault;
use YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy\TextCampaignNetworkStrategyMaximumClicks;
use YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy\TextCampaignNetworkStrategyMaximumConversionRate;
use YandexDirect\Fields\Campaign\TextCampaignNetworkStrategy\TextCampaignNetworkStrategyWeeklyClickPackage;

final class TextCampaignNetworkStrategyAdd
{
    public const BIDDING_STRATEGY_TYPES = [
        'AVERAGE_CPA' => 'AVERAGE_CPA',
        'AVERAGE_CPC' => 'AVERAGE_CPC',
        'AVERAGE_ROI' => 'AVERAGE_ROI',
        'MAXIMUM_COVERAGE' => 'MAXIMUM_COVERAGE',
        'NETWORK_DEFAULT' => 'NETWORK_DEFAULT',
        'SERVING_OFF' => 'SERVING_OFF',
        'WB_MAXIMUM_CLICKS' => 'WB_MAXIMUM_CLICKS',
        'WB_MAXIMUM_CONVERSION_RATE' => 'WB_MAXIMUM_CONVERSION_RATE',
        'WEEKLY_CLICK_PACKAGE' => 'WEEKLY_CLICK_PACKAGE',
    ];
    private string $biddingStrategyType;
    private BaseTextCampaignNetworkStrategy $textCampaignNetworkStrategy;

    public function __construct(string $biddingStrategyType, BaseTextCampaignNetworkStrategy $textCampaignNetworkStrategy)
    {
        $this->biddingStrategyType = $biddingStrategyType;
        $this->textCampaignNetworkStrategy = $textCampaignNetworkStrategy;
        $this->checkCompatibility($biddingStrategyType, $textCampaignNetworkStrategy);
    }

    private function checkCompatibility(string $biddingStrategyType, BaseTextCampaignNetworkStrategy $textCampaignNetworkStrategy): void
    {
        if (($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['NETWORK_DEFAULT'] && !$textCampaignNetworkStrategy instanceof TextCampaignNetworkStrategyDefault)
        or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['WB_MAXIMUM_CLICKS'] && !$textCampaignNetworkStrategy instanceof TextCampaignNetworkStrategyMaximumClicks)
        or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['WB_MAXIMUM_CONVERSION_RATE'] && !$textCampaignNetworkStrategy instanceof TextCampaignNetworkStrategyMaximumConversionRate)
        or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['AVERAGE_CPC'] && !$textCampaignNetworkStrategy instanceof TextCampaignNetworkStrategyAverageCpc)
        or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['AVERAGE_CPA'] && !$textCampaignNetworkStrategy instanceof TextCampaignNetworkStrategyAverageCpa)
        or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['WEEKLY_CLICK_PACKAGE'] && !$textCampaignNetworkStrategy instanceof TextCampaignNetworkStrategyWeeklyClickPackage)
        or ($biddingStrategyType === self::BIDDING_STRATEGY_TYPES['AVERAGE_ROI'] && !$textCampaignNetworkStrategy instanceof TextCampaignNetworkStrategyAverageRoi)) {
            throw new \InvalidArgumentException(sprintf('Not valid text campaign network strategy for bidding type %s', $biddingStrategyType));
        }
    }

    public function getTextCampaignNetworkStrategy(): BaseTextCampaignNetworkStrategy
    {
        return $this->textCampaignNetworkStrategy;
    }

    public function getBiddingStrategyType(): string
    {
        return $this->biddingStrategyType;
    }
}
