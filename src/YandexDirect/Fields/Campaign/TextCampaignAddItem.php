<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Campaign;

final class TextCampaignAddItem
{
    private TextCampaignStrategyAdd $textCampaignStrategyAdd;

    public function __construct(TextCampaignStrategyAdd $textCampaignStrategyAdd)
    {
        $this->textCampaignStrategyAdd = $textCampaignStrategyAdd;
    }

    public function getTextCampaignStrategyAdd(): TextCampaignStrategyAdd
    {
        return $this->textCampaignStrategyAdd;
    }
}
