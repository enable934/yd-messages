<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Ad;

final class AdAddItem
{
    private int $adGroupId;
    private TextAdAdd $textAd;

    public function __construct(int $adGroupId, TextAdAdd $textAd)
    {
        $this->adGroupId = $adGroupId;
        $this->textAd = $textAd;
    }

    public function getAdGroupId(): int
    {
        return $this->adGroupId;
    }

    public function getTextAd(): TextAdAdd
    {
        return $this->textAd;
    }
}
