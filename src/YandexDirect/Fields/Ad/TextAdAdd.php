<?php

declare(strict_types=1);

namespace YandexDirect\Fields\Ad;

final class TextAdAdd
{
    private string $title;
    private string $text;
    private string $mobile;
    private string $href;

    public function __construct(string $title, string $text, string $mobile, string $href)
    {
        $this->title = $title;
        $this->text = $text;
        $this->mobile = $mobile;
        $this->href = $href;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getMobile(): string
    {
        return $this->mobile;
    }

    public function getHref(): string
    {
        return $this->href;
    }
}
