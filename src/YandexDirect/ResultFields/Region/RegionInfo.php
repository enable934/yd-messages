<?php

declare(strict_types=1);

namespace YandexDirect\ResultFields\Region;

final class RegionInfo
{
    private int $regionId;
    private ?int $parentId;
    private string $regionName;
    private ?string $regionType;

    public function __construct(int $regionId, ?int $parentId, string $regionName, ?string $regionType)
    {
        $this->regionId = $regionId;
        $this->parentId = $parentId;
        $this->regionName = $regionName;
        $this->regionType = $regionType;
    }

    public function getRegionId(): int
    {
        return $this->regionId;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function getRegionName(): string
    {
        return $this->regionName;
    }

    public function getRegionType(): ?string
    {
        return $this->regionType;
    }
}
