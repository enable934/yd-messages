<?php

declare(strict_types=1);

namespace YandexDirect\ResultFields\WordReport;

final class WordItem
{
    private string $phrase;
    private int $shows;

    public function __construct(string $phrase, int $shows)
    {
        $this->phrase = $phrase;
        $this->shows = $shows;
    }

    public function getPhrase(): string
    {
        return $this->phrase;
    }

    public function getShows(): int
    {
        return $this->shows;
    }
}
