<?php

declare(strict_types=1);

namespace YandexDirect\ResultFields\WordReport;

final class WordReportInfo
{
    private string $phrase;
    private array $searchedWith;
    private array $searchedAlso;

    public function __construct(string $phrase, array $searchedWith, array $searchedAlso)
    {
        $this->phrase = $phrase;
        $this->searchedWith = $searchedWith;
        $this->searchedAlso = $searchedAlso;
    }

    /**
     * @return array|WordItem[]
     */
    public function getSearchedAlso(): array
    {
        return $this->searchedAlso;
    }

    /**
     * @return array|WordItem[]
     */
    public function getSearchedWith(): array
    {
        return $this->searchedWith;
    }

    public function getPhrase(): string
    {
        return $this->phrase;
    }
}
