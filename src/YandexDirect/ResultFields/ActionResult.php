<?php

declare(strict_types=1);

namespace YandexDirect\ResultFields;

final class ActionResult
{
    private ?int $id;
    private ?array $warnings;
    private ?array $errors;

    public function __construct(int $id = null, array $warnings = null, array $errors = null)
    {
        $this->id = $id;
        $this->warnings = $warnings;
        $this->errors = $errors;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return array|ExceptionNotification[]|null
     */
    public function getWarnings(): ?array
    {
        return $this->warnings;
    }

    /**
     * @return array|ExceptionNotification[]|null
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }
}
