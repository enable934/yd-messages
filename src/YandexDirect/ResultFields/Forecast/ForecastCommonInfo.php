<?php

declare(strict_types=1);

namespace YandexDirect\ResultFields\Forecast;

final class ForecastCommonInfo
{
    private string $geo;
    private float $min;
    private float $max;
    private float $premiumMin;
    private int $shows;
    private int $clicks;
    private int $firstPlaceClicks;
    private int $premiumClicks;

    public function __construct(string $geo, float $min, float $max, float $premiumMin, int $shows, int $clicks, int $firstPlaceClicks, int $premiumClicks)
    {
        $this->geo = $geo;
        $this->min = $min;
        $this->max = $max;
        $this->premiumMin = $premiumMin;
        $this->shows = $shows;
        $this->clicks = $clicks;
        $this->firstPlaceClicks = $firstPlaceClicks;
        $this->premiumClicks = $premiumClicks;
    }

    public function getGeo(): string
    {
        return $this->geo;
    }

    public function getMin(): float
    {
        return $this->min;
    }

    public function getMax(): float
    {
        return $this->max;
    }

    public function getPremiumMin(): float
    {
        return $this->premiumMin;
    }

    public function getShows(): int
    {
        return $this->shows;
    }

    public function getClicks(): int
    {
        return $this->clicks;
    }

    public function getFirstPlaceClicks(): int
    {
        return $this->firstPlaceClicks;
    }

    public function getPremiumClicks(): int
    {
        return $this->premiumClicks;
    }
}
