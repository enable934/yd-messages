<?php

declare(strict_types=1);

namespace YandexDirect\ResultFields\Forecast;

final class BannerPhraseInfo
{
    private string $phrase;
    private string $isRubric;
    private float $min;
    private float $max;
    private float $premiumMin;
    private float $premiumMax;
    private int $shows;
    private int $clicks;
    private int $firstPlaceClicks;
    private int $premiumClicks;
    private float $CTR;
    private float $firstPlaceCTR;
    private float $premiumCTR;
    private string $currency;

    public function __construct(
        string $phrase,
        string $isRubric,
        float $min,
        float $max,
        float $premiumMin,
        float $premiumMax,
        int $shows,
        int $clicks,
        int $firstPlaceClicks,
        int $premiumClicks,
        float $CTR,
        float $firstPlaceCTR,
        float $premiumCTR,
        string $currency
    ) {
        $this->phrase = $phrase;
        $this->isRubric = $isRubric;
        $this->min = $min;
        $this->max = $max;
        $this->premiumMin = $premiumMin;
        $this->premiumMax = $premiumMax;
        $this->shows = $shows;
        $this->clicks = $clicks;
        $this->firstPlaceClicks = $firstPlaceClicks;
        $this->premiumClicks = $premiumClicks;
        $this->CTR = $CTR;
        $this->firstPlaceCTR = $firstPlaceCTR;
        $this->premiumCTR = $premiumCTR;
        $this->currency = $currency;
    }

    public function getPhrase(): string
    {
        return $this->phrase;
    }

    public function getIsRubric(): string
    {
        return $this->isRubric;
    }

    public function getMin(): float
    {
        return $this->min;
    }

    public function getMax(): float
    {
        return $this->max;
    }

    public function getPremiumMin(): float
    {
        return $this->premiumMin;
    }

    public function getPremiumMax(): float
    {
        return $this->premiumMax;
    }

    public function getShows(): int
    {
        return $this->shows;
    }

    public function getClicks(): int
    {
        return $this->clicks;
    }

    public function getFirstPlaceClicks(): int
    {
        return $this->firstPlaceClicks;
    }

    public function getPremiumClicks(): int
    {
        return $this->premiumClicks;
    }

    public function getCTR(): float
    {
        return $this->CTR;
    }

    public function getFirstPlaceCTR(): float
    {
        return $this->firstPlaceCTR;
    }

    public function getPremiumCTR(): float
    {
        return $this->premiumCTR;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
